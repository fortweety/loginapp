from rest_framework import routers
from . import views
from django.conf.urls import url, include


router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet)

urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^authenticate/', views.CustomObtainAuthToken.as_view()),
]
